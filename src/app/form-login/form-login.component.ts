import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

// Social Media Import
import { AuthService, FacebookLoginProvider } from "angular-6-social-login";
import { ActivatedRoute, Router } from '@angular/router';
import { from } from 'rxjs';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent implements OnInit {

  constructor (private socialAuthSercvice: AuthService, 
                private route: ActivatedRoute, 
                  private router: Router, 
                    private userService: UserService) { }
  
  public user;
  returnUrl : string;


  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;

    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }

    this.socialAuthSercvice.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + "Sign In Data" , userData );
        this.user = userData;
        this.userService.setUserLogin();
        return this.router.navigateByUrl(
          this.returnUrl
        );
      }
    )
    
    
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParamMap['returnUrl'] || '/home';
  }

}
