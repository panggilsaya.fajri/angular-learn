import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { AuthguardGuard } from './authguard.guard';


const routes: Routes = [
  { path: 'home', canActivate: [AuthguardGuard], component: HomepageComponent },
  { path: '', component: FormLoginComponent}
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ], 
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
