import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private user: UserService, private rout: Router) { }

  ngOnInit() {
  }

  LogOut() {
    this.rout.navigate(['/']);
  }

}
