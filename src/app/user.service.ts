import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { 
    this.isUserLogedIn = false
  }

  setUserLogin() {
    this.isUserLogedIn = true
  }

  getUserLogin() {
    return this.isUserLogedIn;
  }

  public isUserLogedIn;
  public userName;

}
